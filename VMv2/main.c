#include "virtualMachine/virtualMachine.h"
#include "virtualMachine/SMBler.h"
#include "virtualMachine/preAsmData.h"


int isDebug = 0;

int main(int argc, char *argv[])
{   
    
    if(isDebug == 0)
    {
        if(argc == 2)
        {
            char* imagePath = assembleToFile(argv[1]);
            loadImage(imagePath);
        }
        else
        {
            char* imagePath = assembleToFile("programs/rps");
            loadImage(imagePath);
        }
    }
    else
    {
        printf("%d\n", (2+3-3)%3);
    }
    
  
    return 0;
}