#include"stringHelper.h"
#include<stdbool.h>
#include<strings.h>
#include<ctype.h>

bool isStrDigit(char* str)
{
    int len = strlen(str);
    for(int i = 0; i < len; i++)
    {
        if (!isdigit(str[i]))
            return false;
    }
    return true;  
}
