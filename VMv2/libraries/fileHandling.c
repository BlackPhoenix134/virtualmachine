#include"fileHandling.h"
#include<stdlib.h>
#include<string.h>
#include<stdint.h>
#include<stdio.h>
#include<io.h>

char* fgetLine(FILE* stream, char* buffer, int size)
{
    char* retVal;
    do
    {
        retVal = fgets(buffer, 255, stream);
    } while (!feof(stream) && retVal[0] == '\n');

    int length = strlen(buffer);
    if(buffer[length-1] == '\n')
        buffer[length-1] = '\0';
    return retVal;
}