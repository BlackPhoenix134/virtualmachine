#include"instructionCoder.h"
#include<stdint.h>
#include<stdbool.h>

void setOpCode(uint32_t* instruction, uint32_t opcode);
void setBitsFrom(uint32_t* destination, uint32_t source, uint32_t fromBit);
void setInstructionFlagBit(uint32_t* instruction, uint32_t value);

//32 bit system
//ToDo: implement swap (Big to little endian etc)
//ToDo: implement sign extend
/* Instruction types
    OP [31-26] | Empty    [25 - 0]
    OP [31-26] | Value    [25 - 0]
    OP [31-26] | Register [25-22] | Empty    [21-0]
    OP [31-26] | Register [25-22] | Value    [21-0]
    OP [31-26] | Register [25-22] | Register [21-18]   |  Empty [17-0]  
    OP [31-26] | Register [25-22] | Register [21-18]   |  Flag  [17]    | Value [16-0]                             Flag = 0
    OP [31-26] | Register [25-22] | Register [21-18]   |  Flag  [17]    | Empty [16-4]  | Register  [3-0]          Flag = 1
*/

typedef enum InstructionEncodigTypes{
    OPCODE, REGISTER, IMMEDIATE, MEMORY
} InstructionEncodigTypes;

typedef struct InstructionEncodigPart {
   InstructionEncodigTypes type;
   uint32_t value;
} InstructionEncodigPart;


uint32_t encodeInstruction(InstructionEncodigPart* parts, int size)
{
    uint32_t encodedInstruction = 0;
    InstructionEncodigPart currentPart;
    for(int i = 0; i < size; i++)
    {
        currentPart = parts[i];
        if(currentPart.type == OPCODE)
        {
            setOpCode(&encodedInstruction, currentPart.value);
        }
        else if(currentPart.type == REGISTER)
        {
            if(i == 1)
            {
                setBitsFrom(&encodedInstruction, currentPart.value, 22);
            }
            else if(i == 2)
            {
                setBitsFrom(&encodedInstruction, currentPart.value, 18);
            }
            else if(i == 3)
            {
                setInstructionFlagBit(&encodedInstruction, 1);
                setBitsFrom(&encodedInstruction, currentPart.value, 0);
            }
        }
        else if(currentPart.type == IMMEDIATE)
        {
            if(i == 1 || i == 2)
            {
                setBitsFrom(&encodedInstruction, currentPart.value, 0);
            }
            else if(i == 3)
            {
                setInstructionFlagBit(&encodedInstruction, 0);
                setBitsFrom(&encodedInstruction, currentPart.value, 0);
            }
        }
    }
    return encodedInstruction;
}

uint32_t getOpCode(uint32_t instruction)
{
    return instruction >> 26;
}

void setOpCode(uint32_t* instruction, uint32_t opcode)
{
    setBitsFrom(instruction, opcode, 26);
}

void setInstructionFlagBit(uint32_t* instruction, uint32_t value)
{
    setBitsFrom(instruction, value, 17);
}

void setBitsFrom(uint32_t* destination, uint32_t source, uint32_t fromBit)
{
    (*destination) |= (source << fromBit); 
} 

uint32_t getBitsRange(uint32_t instruction, int fromBit, int count)
{
    return (((1 << count) - 1) & (instruction >> (fromBit))); 
}

uint32_t getRegisterBits(uint32_t instruction, int fromBit)
{
    return getBitsRange(instruction, fromBit, 4);
}

uint32_t getInstructionFlag(uint32_t instruction)
{
    return getBitsRange(instruction, 17, 1);
}