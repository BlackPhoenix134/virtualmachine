#include"virtualMachine.h"
#include"instructionCoder.h"
#include"../libraries/stringHelper.h"
#include"asmMetaData.h"
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <time.h>
typedef void (*Handler)(void);


void getRandom();

#define STKP (registers[SP])
#define JSTKP (registers[JSP])
#define INSP (registers[IP])

#define GE_REG_1 (registers[R1])
#define GE_REG_2 (registers[R2])
#define GE_REG_3 (registers[R3])
#define GE_REG_4 (registers[R4])

#define DYN_REG_1 (registers[R5])
#define DYN_REG_2 (registers[R6])

#define ARG_REG_1 (registers[A1])
#define ARG_REG_2 (registers[A2])
#define OUT_REG_1 (registers[O1])

#define REG1_BIT 22
#define REG2_BIT 18
#define REG3_BIT 0

int fetchInstruction();
void initialize();
void evaluateInstruction(int instructionCounter);

//ToDo: why is PRT (print) not out, if IN is input ???? Fabian, U STUPID?
enum InstructionSet {
    HLT, PSH, POP, ADD, LD, LMR, MOV, JMP, NOP, RET, POR, IF, IFN, PRT, SUB, IN, SMR, LMI, SMI, MOD, TRP
};

//ToDo: Research: using dynamic register R6 seems broken
typedef enum {
    //keeps value 
    R1, R2, R3, R4, 
    //dynamic
    R5, R6,
    //for vm
    A1, A2, 
    O1,
    SP, IP, JSP,
    REG_SIZE
} Registers;

typedef enum {
    CLS,
    TRAP_SIZE
} TrapCode;


//2^16 memory places
char inputBuffer[50];
uint32_t memory[2048];          //ToDo: if anything breaks, reset uint32_t to int
uint32_t stack[1024];           //ToDo: if anything breaks, reset uint32_t to int
int jumpStack[1024];

int registers[REG_SIZE];

Handler trapJumpTable[1] = {
    getRandom
};

bool shouldRun = true;

uint32_t* instructions;

int fetchInstruction() {
    return instructions[INSP];
}

void evaluateInstruction(int instr) 
{
    //opcode first 6 bit
    uint32_t opCode = getOpCode(instr);
    switch (opCode)
    {
        case HLT: 
        {
            shouldRun = false;
            printf("\n-------------------------------\nExited\n");
            for(int i = 0; i < REG_SIZE; i++)
            {
                printf("R%d: %d\n", i+1, registers[i]);
            }
            printf("\nStack:\n");
            for(int i = STKP; i >= 0; i--)
            {
                printf("%d: %d\n", i, stack[i]);
            }

            printf("\nJumpStack:\n");
            for(int i = JSTKP; i >= 0; i--)
            {
                printf("%d: %d\n", i, jumpStack[i]);
            }
            break;
        }
        case PSH:
        {
            STKP++;
            stack[STKP] = registers[getRegisterBits(instr, REG1_BIT)];
            break;
        }
        case POP:
        {
            STKP--;
            break;
        }
        case MOD:
        {
            DYN_REG_1 = getInstructionFlag(instr);
            if(DYN_REG_1 == 0)
            {
                registers[getRegisterBits(instr, REG1_BIT)] =
                    registers[getRegisterBits(instr, REG2_BIT)] % getBitsRange(instr, 0, 16);
            }
            else
            {
                registers[getRegisterBits(instr, REG1_BIT)] =
                    registers[getRegisterBits(instr, REG2_BIT)] % registers[getRegisterBits(instr, REG3_BIT)];
            }
            break;
        }
        case ADD:
        {
            DYN_REG_1 = getInstructionFlag(instr);
            if(DYN_REG_1 == 0)
            {
                registers[getRegisterBits(instr, REG1_BIT)] =
                    registers[getRegisterBits(instr, REG2_BIT)] + getBitsRange(instr, 0, 16);
            }
            else
            {
                registers[getRegisterBits(instr, REG1_BIT)] =
                    registers[getRegisterBits(instr, REG2_BIT)] +  registers[getRegisterBits(instr, REG3_BIT)];
            }
            break;
        }
        case SUB:
        {
            if(getInstructionFlag(instr) == 0)
            {
                registers[getRegisterBits(instr, REG1_BIT)] =
                    registers[getRegisterBits(instr, REG2_BIT)] - getBitsRange(instr, 0, 16);
            }
            else
            {
                registers[getRegisterBits(instr, REG1_BIT)] =
                    registers[getRegisterBits(instr, REG2_BIT)] - registers[getRegisterBits(instr, REG3_BIT)];

            }
            break;
        }
        case LD:
        {
            registers[getRegisterBits(instr, REG1_BIT)] = getBitsRange(instr, 0, 22);
            break;
        }
        case LMR:
        {
            registers[getRegisterBits(instr, REG1_BIT)] = memory[registers[getRegisterBits(instr, REG2_BIT)]];
            break;
        }
        case SMR:
        {
            memory[registers[getRegisterBits(instr, REG2_BIT)]] = registers[getRegisterBits(instr, REG1_BIT)];
            break;
        }
        case LMI:
        {
            registers[getRegisterBits(instr, REG1_BIT)] = memory[getBitsRange(instr, 0, 22)];
            break;
        }
        case SMI:
        {
            memory[getBitsRange(instr, 0, 22)] = registers[getRegisterBits(instr, REG1_BIT)];
            break;
        }
        case MOV:
        {
            registers[getRegisterBits(instr, 22)] = registers[getRegisterBits(instr, 18)];
            break;
        }
        case JMP:
        {
            DYN_REG_1 = getBitsRange(instr, 0, 26);
            JSTKP++;
            jumpStack[JSTKP] = INSP;
            INSP = DYN_REG_1 - 1;   //-1 because of INSP increment after every eval
            break;
        }
        case NOP: //no operation
        {
            break;
        }
        case RET:
        {
            DYN_REG_1 = jumpStack[JSTKP];
            INSP = DYN_REG_1;      //not -1, because actually you want to get to the JMP + 1, since instrptr gets 
            JSTKP--;              //increment after this, its JMP + 1 (prevents endless loop onto jump again)
            break;
        }
        case POR: 
        {
            registers[getRegisterBits(instr, 22)] = stack[STKP];
            STKP--;
            break;
        }
        case PRT: 
        {
            if(getBitsRange(instr, 0, 22) == 0)
            {
                printf("%d", registers[getRegisterBits(instr, REG1_BIT)]);
            }
            else if(getBitsRange(instr, 0, 22) == 1)
            {
                printf("%c", registers[getRegisterBits(instr, REG1_BIT)]);
            }
            break;
        }
        case IF:
        {
            DYN_REG_1 = getInstructionFlag(instr);
            if(DYN_REG_1 == 0)
            {
                if(registers[getRegisterBits(instr, REG1_BIT)] == registers[getRegisterBits(instr, REG2_BIT)])
                {
                    INSP = getBitsRange(instr, 0, 16) - 1;
                }
            }
            else
            {
               printf("IF jump to register not supported");
            }
            break;
        }
        case IFN:
        {
            DYN_REG_1 = getInstructionFlag(instr);
            if(DYN_REG_1 == 0)
            {
                if(registers[getRegisterBits(instr, REG1_BIT)] != registers[getRegisterBits(instr, REG2_BIT)])
                {
                    INSP = getBitsRange(instr, 0, 16) - 1;
                }
            }
            else
            {
               printf("IFN Jump to register not supported");
            }
            break;
        }
        case IN:
        {
            inputBuffer[0] = '\0';
            scanf("%s", inputBuffer);
            if(isStrDigit(inputBuffer))
            {
                registers[getRegisterBits(instr, REG1_BIT)] = atoi(inputBuffer);
            }
            else
            {
              
                registers[getRegisterBits(instr, REG1_BIT)] = ((int)inputBuffer[0]);
            }
            break;
        }
        case TRP:
        {
            int test = getBitsRange(instr, 0, 26);
            trapJumpTable[test]();
            break;
        }
        default:
        {
            shouldRun = false;
            printf("Bad op code %u\n", opCode);
            break;
        }
    }
}

void initialize() 
{
    srand(time(NULL));  

    STKP = -1;
    JSTKP = -1;
    INSP = 0;
}

//ToDo: multithreaded   
int runMachine(uint32_t* program) {
    instructions = program;
    int overflow = 0;
    initialize();
    printf("Image loaded\n-------------------------------\n");
    while (shouldRun) 
    {
        evaluateInstruction(fetchInstruction());
        INSP++;
        
        if(overflow++ > 100000)
             return -1;
    }
}

void loadImage(char* fileName)
{
    FILE* file;
    file = fopen(fileName, "rb");
  
    asmMetaData* fileMetaData = createAsmMetaData();
    fread(fileMetaData, sizeof(asmMetaData), 1, file);
    
    uint32_t* instructions = malloc(sizeof(uint32_t) * fileMetaData->instructionCount);
    fread(instructions, sizeof(uint32_t) * fileMetaData->instructionCount, 1, file);
    uint32_t currentInstruction;
    free(fileMetaData);
    fclose(file);

    runMachine(instructions);
}

//Trap code implementations
void getRandom()
{
    OUT_REG_1 = (rand() % (ARG_REG_2 - ARG_REG_1)) + ARG_REG_1;
}
//