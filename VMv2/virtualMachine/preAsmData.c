#include"preAsmData.h"
#include <stdlib.h>
#include <string.h>
#include<stdint.h>
typedef struct preAsmData
{
    char labelName[100];
    int instrJump;
} preAsmData;

preAsmData* createPreAsmData()
{
   return malloc(sizeof(struct preAsmData));
}

preAsmData* createPreAsmDataFrom(char* labelName, int instrJump)
{
   preAsmData* data = createPreAsmData();
   strcpy(data->labelName, labelName);
   data->instrJump = instrJump;
   return data;
}

void freePreAsmData(preAsmData* data)
{
    free(data);
}