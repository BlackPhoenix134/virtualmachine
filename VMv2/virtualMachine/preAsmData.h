#ifndef PRE_ASM_DATA_H
#define PRE_ASM_DATA_H
#include"preAsmData.c"
preAsmData* createPreAsmData();
preAsmData* createPreAsmDataFrom(char* labelName, int instrJump);
void freePreAsmData(preAsmData* data);

#endif
