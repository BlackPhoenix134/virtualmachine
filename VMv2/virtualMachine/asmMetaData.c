#include"asmMetaData.h"
#include <stdlib.h>
#include <string.h>
#include<stdint.h>
typedef struct asmMetaData
{
    int instructionCount;
} asmMetaData;

asmMetaData* createAsmMetaData()
{
   return malloc(sizeof(struct asmMetaData));
}

asmMetaData* createAsmMetaDataWith(int instructionCount)
{
    asmMetaData* data = createAsmMetaData();
    data->instructionCount = instructionCount;
    return data;
}

void freeAsmMetaData(asmMetaData* data)
{
    free(data);
}