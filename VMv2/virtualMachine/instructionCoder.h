#ifndef INSTRUCTION_CODER_H
#define INSTRUCTION_CODER_H
#include"instructionCoder.c"
uint32_t encodeInstruction(InstructionEncodigPart* parts, int size);
uint32_t getBitsRange(uint32_t instruction, int fromBit, int count);
uint32_t getRegisterBits(uint32_t instruction, int fromBit);
uint32_t getInstructionFlag(uint32_t instruction);
bool isStrDigit(char* str);


#endif