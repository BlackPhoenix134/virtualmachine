#ifndef ASM_META_DATA_H
#define ASM_META_DATA_H
#include"asmMetaData.c"


asmMetaData* createAsmMetaData();
asmMetaData* createAsmMetaDataWith(int instructionCount);
void freeAsmMetaData(asmMetaData* data);

#endif
