#include"SMbler.h"
#include"asmMetaData.h"
#include"preAsmData.h"
#include"instructionCoder.h"
#include"../libraries/fileHandling.h"
#include"../libraries/stringHelper.h"
#include<stdlib.h>
#include<string.h>
#include<stdint.h>
#include<stdbool.h>
#include<stdio.h>
#include <ctype.h>

#include <fcntl.h>
#include <io.h>
#include <sys\stat.h>

int getOpcodeIdx(char *name);
int getRegisterIdx(char *name);
void runPreAssembler(FILE* file);
int getPreAsmDataFor(char* str);

#define alloc(name, size) name = malloc(sizeof(*name) * size)

#define PRE_ASM_DEF_SIZE 1
char* preassemblerDefinitions[PRE_ASM_DEF_SIZE] = {
    "LBL"
};

//ToDo: find a better way for this
#define INSTR_NAME_SIZE 21
char* instructionNames[INSTR_NAME_SIZE] = {
    "HLT", "PSH", "POP", "ADD", "LD", "LMR", "MOV", "JMP", "NOP", "RET", "POR", "IF", "IFN", "PRT", "SUB", "IN", "SMR", "LMI", "SMI", "MOD", "TRP"
};  

#define REG_NAME_SIZE 9
char* registerNames[REG_NAME_SIZE] = {
    "R1", "R2", "R3", "R4", "R5", "R6", "A1", "A2", "O1"
};

typedef struct preAsmDataStorage {
    int dataLength;
    int instructionCount;
    preAsmData** data;
} preAsmDataStorage;

preAsmDataStorage preAsmStorage = {0};

uint32_t* assemble(char* fileName, int* outInstrCount)
{
    char realFileName[255];
    strcpy(realFileName, fileName);
    strcat(realFileName, ".vmi");
    printf("Starting assembly\n");

    //int instructionSpace = 4;

   // uint32_t* compiledInstructions = (uint32_t*)malloc(sizeof(*compiledInstructions) * instructionSpace);

    char lineBuffer[255];
    bool isEOL = false;
    int i = 0;
    FILE* file = fopen(realFileName, "r");
    runPreAssembler(file);
    fseek(file, 0, SEEK_SET);


    uint32_t* encodedInstructions;
    alloc(encodedInstructions, preAsmStorage.instructionCount);  
    
    InstructionEncodigPart instructionParts[4] = {0};
    int instructionPartSize;
    char* delim = " ";
    while(!feof(file) && fgetLine(file, lineBuffer, 255))
    {
        //ToDo: remove shit underengineerd not thought out code
        instructionPartSize = 0;
        char* currToken = strtok(lineBuffer, delim);
        while (currToken) 
        {
            int instrValue = 0;
            if(strcmp("LBL", currToken) == 0)
            {
                currToken = strtok(NULL, delim);
                instructionParts[instructionPartSize].type = OPCODE;
                instructionParts[instructionPartSize].value = getOpcodeIdx("NOP");
            }
            else if(strcmp("JMP", currToken) == 0)
            {
                instructionParts[instructionPartSize].type = OPCODE;
                instructionParts[instructionPartSize].value = getOpcodeIdx("JMP");
                instructionPartSize++;

                instructionParts[instructionPartSize].type = IMMEDIATE;
                
                currToken = strtok(NULL, delim);
                if(isStrDigit(currToken) == true)
                {
                    instructionParts[instructionPartSize].value = atoi(currToken);
                }
                else
                {
                    int labelInstr = getPreAsmDataFor(currToken);
                    if(labelInstr == -1)
                    {
                        printf("Error: label %s not found", currToken);
                        exit;
                    }
                    instructionParts[instructionPartSize].value = labelInstr;
                }

            }
            else if(strcmp("IF", currToken) == 0)
            {
                instructionParts[instructionPartSize].type = OPCODE;
                instructionParts[instructionPartSize].value = getOpcodeIdx("IF");
                instructionPartSize++;

                currToken = strtok(NULL, delim);
                instrValue = getRegisterIdx(currToken);
                instructionParts[instructionPartSize].type = REGISTER;
                instructionParts[instructionPartSize].value = instrValue;
                instructionPartSize++;
                currToken = strtok(NULL, delim);
                instrValue = getRegisterIdx(currToken);
                instructionParts[instructionPartSize].type = REGISTER;
                instructionParts[instructionPartSize].value = instrValue;
                instructionPartSize++;

                currToken = strtok(NULL, delim);
                instructionParts[instructionPartSize].type = IMMEDIATE;
                if(isStrDigit(currToken) == true)
                {
                    instructionParts[instructionPartSize].value = atoi(currToken);
                }
                else
                {
                    int labelInstr = getPreAsmDataFor(currToken);
                    if(labelInstr == -1)
                    {
                        printf("Error: label %s not found", currToken);
                        exit;
                    }
                    instructionParts[instructionPartSize].value = labelInstr;
                }

            }
            else if(strcmp("IFN", currToken) == 0)
            {
                instructionParts[instructionPartSize].type = OPCODE;
                instructionParts[instructionPartSize].value = getOpcodeIdx("IFN");
                instructionPartSize++;

                currToken = strtok(NULL, delim);
                instrValue = getRegisterIdx(currToken);
                instructionParts[instructionPartSize].type = REGISTER;
                instructionParts[instructionPartSize].value = instrValue;
                instructionPartSize++;
                currToken = strtok(NULL, delim);
                instrValue = getRegisterIdx(currToken);
                instructionParts[instructionPartSize].type = REGISTER;
                instructionParts[instructionPartSize].value = instrValue;
                instructionPartSize++;

                currToken = strtok(NULL, delim);
                instructionParts[instructionPartSize].type = IMMEDIATE;
                if(isStrDigit(currToken) == true)
                {
                    instructionParts[instructionPartSize].value = atoi(currToken);
                }
                else
                {
                    int labelInstr = getPreAsmDataFor(currToken);
                    if(labelInstr == -1)
                    {
                        printf("Error: label %s not found", currToken);
                        exit;
                    }
                    instructionParts[instructionPartSize].value = labelInstr;
                }

            }
            else if((instrValue = getOpcodeIdx(currToken)) >= 0)
            {
                instructionParts[instructionPartSize].type = OPCODE;
                instructionParts[instructionPartSize].value = instrValue;
            }
            else if((instrValue = getRegisterIdx(currToken)) >= 0)
            {
                instructionParts[instructionPartSize].type = REGISTER;
                instructionParts[instructionPartSize].value = instrValue;
            }
            else if(currToken[0] == '*')
            {
                //ToDo: ( remove first * of string, then atoi)
                printf("MEMORY * NOT IMPLEMENTED");
                instrValue = atoi(currToken);
                instructionParts[instructionPartSize].type = MEMORY;
                instructionParts[instructionPartSize].value = instrValue;
            }
            else 
            {
                instrValue = atoi(currToken);
                instructionParts[instructionPartSize].type = IMMEDIATE;
                instructionParts[instructionPartSize].value = instrValue;
            }
            instructionPartSize++;      
            currToken = strtok(NULL, delim);
        }
        uint32_t encodedInstruction = encodeInstruction(instructionParts, instructionPartSize);
        encodedInstructions[i] = encodedInstruction;
        i++;
    }
  
    fclose(file);
    free(preAsmStorage.data);
    printf("Assembled\n");
    *outInstrCount = i;
    return encodedInstructions;
}

char* assembleToFile(char* fileName)
{
    int instrCount = 0;
    uint32_t* instructions = assemble(fileName, &instrCount);

    char *outFileName = malloc(255);
    strcpy(outFileName, fileName);
    strcat(outFileName, ".bin");
    
    FILE* file;
    file = fopen(outFileName, "wb");
    int i = 0;
    uint32_t currentInstruction;
    do
    {
        currentInstruction = instructions[i];
        i++;
    } while(i < instrCount);

    asmMetaData* fileMetaData = createAsmMetaDataWith(i);
    fwrite(fileMetaData, sizeof(struct asmMetaData), 1, file); 
    fwrite(instructions, sizeof(uint32_t) * fileMetaData->instructionCount, 1, file);
   
    free(fileMetaData);
    fclose(file);
    return outFileName;
}

int getRegisterIdx(char *name)
{
    for(int i = 0; i < REG_NAME_SIZE; i++)
    {
        if(strcmp(registerNames[i], name) == false)
        {
            return i;
        }
    }
    return -1;
}

int getOpcodeIdx(char *name)
{
    for(int i = 0; i < INSTR_NAME_SIZE; i++)
    {
        if(strcmp(instructionNames[i], name) == false)
        {
            return i;
        }
    }
    return -1;
}

void runPreAssembler(FILE* file)
{
    free(preAsmStorage.data);
    preAsmStorage.dataLength = 0;
    preAsmStorage.instructionCount = 0;
    alloc(preAsmStorage.data, 150);

    printf("Running pre assembler\n");
    char lineBuffer[255];
    char* currToken;
    char* delim = " ";

    int currentInstructionCount = 0;
    int dataCounter = 0;

    while(fgetLine(file, lineBuffer, 255)) 
    {
        char* currToken = strtok(lineBuffer, delim);
        if(strcmp(currToken, "LBL") == 0) 
        {
            currToken = strtok(NULL, delim); //label name
            preAsmData* data = createPreAsmDataFrom(currToken, currentInstructionCount);
            preAsmStorage.data[dataCounter] = data;
            dataCounter++;
        }
        currentInstructionCount++;
    }
    preAsmStorage.dataLength = dataCounter;
    preAsmStorage.instructionCount = currentInstructionCount;
}

int getPreAsmDataFor(char* str)
{
    int count = 0;
    preAsmData* currData;
    for(int i = 0; i < preAsmStorage.dataLength; i++)
    {
        currData = preAsmStorage.data[i];
        if(strcmp(str, currData->labelName) == 0)
        {
            return currData->instrJump;
        }
    }
    return -1;
}