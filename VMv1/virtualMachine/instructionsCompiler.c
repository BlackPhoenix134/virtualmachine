#include"instructionsCompiler.h"
#include"virtualMachine.h"
#include <stdlib.h>
#include <string.h>

int getOpcodeIdx(char *name);

//PSH, ADDS, ADDR, POP, ADDI, HLT, MOV, LOAD, RTS, STR, IFR, IFNR, IFI, IFNI, JUMP, LOGS, LOGR, NOP
//ToDo: find a better way for this
#define INSTR_NAME_SIZE 18
char* instructionNames[INSTR_NAME_SIZE] = {
    "PSH", "ADDS", "ADDR", "POP", "ADDI", "HLT", "MOV", "LOAD", "RTS", "STR", "IFR", "IFNR", "IFI", "IFNI", "JUMP", "LOGS", "LOGR", "NOP"
};


int* assemble(char* fileName)
{
    //Assume 10 as max instruction word length
    int* compiledInstructions;
    int instructionSpace = 4;
    compiledInstructions = malloc(sizeof(*compiledInstructions) * instructionSpace);
    FILE* file;

    char currInstruction[10] = {0};
    int i = 0;
    file = fopen(fileName, "r");
    printf("Starting compilation\n");
    while(!feof(file))
    {
        fscanf(file, "%9s", currInstruction);
        int instrValue =  getOpcodeIdx(currInstruction);
        if(instrValue == -1)
            instrValue = atoi(currInstruction);
     
        
        compiledInstructions[i++] = instrValue;
        if (i >= instructionSpace) {
            instructionSpace *= 4;
            compiledInstructions = realloc(compiledInstructions, sizeof(*compiledInstructions) * instructionSpace);
            printf("Instruction space extended\n");
        }
    }
    if(compiledInstructions[i] != HLT)
    {
        //no need to realloc, as 1 space at the end is always provided as
        //space increase occures at i >= insructionSpace, not i > instructionSpace
        compiledInstructions[i] = HLT;
    }
    printf("Compiled\n");
    return compiledInstructions;
}

int getOpcodeIdx(char *name)
{
    for(int i = 0; i < INSTR_NAME_SIZE; i++)
    {
        if(strcmp(instructionNames[i], name) == false)
        {
            return i;
        }
    }
    return -1;
}