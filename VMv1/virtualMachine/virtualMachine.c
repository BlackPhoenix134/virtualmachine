#include"virtualMachine.h"
#include <stdio.h>
#include <stdbool.h>

#define STKP (registers[SP])
#define INSP (registers[IP])
#define VM_REG_1 (registers[R5])
#define VM_REG_2 (registers[R6])
#define VM_REG_3 (registers[R7])
#define VM_REG_4 (registers[R8])

int fetchInstruction();
void initialize();
void evaluateInstruction(int instructionCounter);

//ToDo: Make this a array to map int to strings etc
//ToDo: Rework whole instruction set structure so not every param is an integer
//Use bits and write very op-code into one int. This can be converted to a hex value that has space in an int* in memory,
//so whole programm can be loaded into memory where every instruction is one integer wide, not 1-4 int. 
//=> More hardware near implementation (PC points to exactly one instruction)
//Moreover, this allows overloading of instruction sets
//   ADDI and ADDR can be combined by having 
//   ADD R0 R1 R2 or ADD R0 R1 <value>
//   OP_CODE (6 bit) Destination Reg (4 bit) Source reg1 (4 bit) flag (1 bit) zeros (13 bit) Source reg2 (4 bit)
//   where flag is 1 for immediate value and 0 for register value
//   P_CODE (6 bit) Destination Reg (4 bit) Source reg1 (4 bit) flag (1 bit) immediate value (17 bit)
//ToDo: implement writing to memory (for arrays etc,..) (Maybe in VMv2)
enum InstructionSet {
    PSH, ADDS, ADDR, POP, ADDI, HLT, MOV, LOAD, RTS, STR, IFR, IFNR, IFI, IFNI, JUMP, LOGS, LOGR, NOP
};

typedef enum {
    //for user 
    R1, R2, R3, R4,
    //for vm
    R5, R6, R7, R8,
    SP, IP,
    REG_SIZE
} Registers;
int stack[256];

int registers[REG_SIZE];

bool shouldRun = true;
bool isJumping = false;

int* instructions;

int fetchInstruction() 
{
    return instructions[INSP];
}

void printStack()
{
    for(int i = STKP; i >= 0; i--)
        printf("Stack: %d: %d\n", i, stack[i]);
}
void printRegister()
{
    for(int i = 0; i < REG_SIZE; i++)
        printf("Register: %d: %d\n", i, registers[i]);
}

//In real environment, every instruction is one line -> 32/64 bit. So ADD, r1, r2, r3 = integer, not integer*4
void evaluateInstruction(int instr) 
{
    isJumping = false;
    switch (instr) 
    {
        case HLT: 
        {
            shouldRun = false;
            printf("\nProgram exited\n");
            printf("Stack on exit:\n");
            printStack();
            printf("\nRegister on exit:\n");
            printRegister();
            break;
        }
        case PSH: 
        {
            STKP++; 
            INSP++;
            stack[STKP] = instructions[INSP];
            break;
        }
        case POP: 
        {
            STKP--;
            int valPopped = stack[STKP];
            break;
        }
        case ADDS: 
        {
            DYN_REG_1 = stack[STKP];
            STKP--;
            DYN_REG_2 = stack[STKP];
            VM_REG_3 = DYN_REG_1 + DYN_REG_2;
            stack[STKP] = VM_REG_3;
            break;
        }
        case ADDR: 
        {
            DYN_REG_1 = registers[instructions[INSP + 1]] + registers[instructions[INSP + 2]];
            registers[instructions[INSP + 3]] = DYN_REG_1;
            INSP += 3;
            break;
        }
        case ADDI: 
        {
            DYN_REG_1 = registers[instructions[INSP + 1]] + instructions[INSP + 2];
            registers[instructions[INSP + 3]] = DYN_REG_1;
            INSP += 3;
            break;
        }
        case MOV: 
        {
            registers[instructions[INSP + 2]] = registers[instructions[INSP + 1]];
            INSP += 2;
            break;
        }
        case LOAD: 
        {
            registers[instructions[INSP + 1]] = instructions[INSP + 2];
            INSP += 2;
            break;
        }
        case RTS: 
        {
            INSP++;
            STKP++;
            stack[STKP] = registers[instructions[INSP]];
            break;
        }
        case STR: 
        {
            INSP++;
            registers[instructions[INSP]] = stack[STKP];
            break;
        }
        case IFR: 
        {
            if(registers[instructions[INSP + 1]] == registers[instructions[INSP + 2]]) 
            {
                INSP = instructions[INSP + 3];
                isJumping = true;
            }
            else
            {
                INSP += 3;
            }
            break;
        }
        case IFNR: 
        {
            if(registers[instructions[INSP + 1]] != registers[instructions[INSP + 2]]) 
            {
                INSP = instructions[INSP + 3];
                isJumping = true;
            }
            else
            {
                INSP += 3;
            }
            break;
        }
        case IFI: 
        {
            if(registers[instructions[INSP + 1]] == instructions[INSP + 2]) 
            {
                INSP = instructions[INSP + 3];
                isJumping = true;
            }
            else
            {
                INSP += 3;
            }
            break;
        }
        case IFNI: 
        {
            if(registers[instructions[INSP + 1]] != instructions[INSP + 2]) 
            {
                INSP = instructions[INSP + 3];
                isJumping = true;
            }
            else
            {
                INSP += 3;
            }
            break;
        }
        case JUMP: 
        {
            INSP = instructions[INSP + 1];
            isJumping = true;
            break;
        }
        case NOP: 
        {
            //No Operation
            break;
        }
        case LOGS: 
        {
            printf("Stack %d: %d\n", STKP, stack[STKP]);
            break;
        }
        case LOGR: 
        {
            INSP++;
            printf("Register %d: %d\n", instructions[INSP], registers[instructions[INSP]]);
        }
    }
}

void initialize() 
{
    STKP = -1;
    INSP = 0;
}


int runMachine(int* program) {
    instructions = program;
    int overflow = 0;
    initialize();
    while (shouldRun) {
        evaluateInstruction(fetchInstruction());
        if(!isJumping)
            INSP++;
        
        if(overflow++ > 100000)
             return -1;
    }
}
