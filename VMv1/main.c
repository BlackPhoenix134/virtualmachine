#include"virtualMachine/virtualMachine.h"
#include"virtualMachine/instructionsCompiler.h"

int main(int argc, char** argv) {
    if (argc <= 1) {
        int* instructions = assemble("programs/prog1.vmi");
        runMachine(instructions);
    }
    else
    {
        char *filename = argv[1];
        int* instructions = assemble(filename);
        runMachine(instructions);
    }
    return 0;
}